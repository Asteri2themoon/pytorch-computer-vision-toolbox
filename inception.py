import torch
import torch.nn as nn
import numpy as np

class InceptionBlockA(nn.Module):
    def __init__(self, input_depth = 384):
        super(InceptionBlockA,self).__init__()

        self.pooling_average = nn.AvgPool2d(kernel_size = 3, stride = 1, padding = 1)
        self.pooling_conv = nn.Conv2d(input_depth,96,1, stride = 1)

        self.conv_x1_conv = nn.Conv2d(input_depth,96,1, stride = 1)

        self.conv_x3_conv1 = nn.Conv2d(input_depth,64,1, stride = 1)
        self.conv_x3_conv2 = nn.Conv2d(64,96,3, padding = 1, stride = 1)

        self.conv_x5_conv1 = nn.Conv2d(input_depth,64,1, stride = 1)
        self.conv_x5_conv2 = nn.Conv2d(64,96,3, padding = 1, stride = 1)
        self.conv_x5_conv3 = nn.Conv2d(96,96,3, padding = 1, stride = 1)

    def forward(self,x):

        out_average = self.pooling_average(x)
        out_average = self.pooling_conv(out_average)

        out_conv_x1 = self.conv_x1_conv(x)

        out_conv_x3 = self.conv_x3_conv1(x)
        out_conv_x3 = self.conv_x3_conv2(out_conv_x3)

        out_conv_x5 = self.conv_x5_conv1(x)
        out_conv_x5 = self.conv_x5_conv2(out_conv_x5)
        out_conv_x5 = self.conv_x5_conv3(out_conv_x5)

        return torch.cat((out_average, out_conv_x1, out_conv_x3, out_conv_x5), 1)

class InceptionBlockB(nn.Module):
    def __init__(self, input_depth = 1024):
        super(InceptionBlockB,self).__init__()

        self.pooling_average = nn.AvgPool2d(kernel_size = 3, stride = 1, padding = 1)
        self.pooling_conv = nn.Conv2d(input_depth,128,kernel_size = 1, stride = 1)

        self.conv_x1_conv = nn.Conv2d(input_depth,384,kernel_size = 1, stride = 1)

        self.conv_x7_conv1 = nn.Conv2d(input_depth,192,kernel_size = 1, stride = 1)
        self.conv_x7_conv2 = nn.Conv2d(192,224,kernel_size = (7,1), padding = (3,0), stride = 1)
        self.conv_x7_conv3 = nn.Conv2d(224,256,kernel_size = (1,7), padding = (0,3), stride = 1)

        self.conv_x15_conv1 = nn.Conv2d(input_depth,192,kernel_size = 1, stride = 1)
        self.conv_x15_conv2 = nn.Conv2d(192,192,kernel_size = (1,7), padding = (0,3), stride = 1)
        self.conv_x15_conv3 = nn.Conv2d(192,224,kernel_size = (7,1), padding = (3,0), stride = 1)
        self.conv_x15_conv4 = nn.Conv2d(224,224,kernel_size = (1,7), padding = (0,3), stride = 1)
        self.conv_x15_conv5 = nn.Conv2d(224,256,kernel_size = (7,1), padding = (3,0), stride = 1)

    def forward(self,x):

        out_average = self.pooling_average(x)
        out_average = self.pooling_conv(out_average)

        out_conv_x1 = self.conv_x1_conv(x)

        out_conv_x7 = self.conv_x7_conv1(x)
        out_conv_x7 = self.conv_x7_conv2(out_conv_x7)
        out_conv_x7 = self.conv_x7_conv3(out_conv_x7)
        
        out_conv_x15 = self.conv_x15_conv1(x)
        out_conv_x15 = self.conv_x15_conv2(out_conv_x15)
        out_conv_x15 = self.conv_x15_conv3(out_conv_x15)
        out_conv_x15 = self.conv_x15_conv4(out_conv_x15)
        out_conv_x15 = self.conv_x15_conv5(out_conv_x15)
        
        return torch.cat((out_average, out_conv_x1, out_conv_x7, out_conv_x15), 1)

class InceptionBlockC(nn.Module):
    def __init__(self, input_depth = 1536):
        super(InceptionBlockC,self).__init__()

        self.pooling_average = nn.AvgPool2d(kernel_size = 3, stride = 1, padding = 1)
        self.pooling_conv = nn.Conv2d(input_depth,256,kernel_size = 1, stride = 1)

        self.conv_x1_conv = nn.Conv2d(input_depth,256,kernel_size = 1, stride = 1)

        self.conv_x3_conv1 = nn.Conv2d(input_depth,384,kernel_size = 1, stride = 1)
        self.conv_x3_conv2_1 = nn.Conv2d(384,256,kernel_size = (1,3), padding = (0,1), stride = 1)
        self.conv_x3_conv2_2 = nn.Conv2d(384,256,kernel_size = (3,1), padding = (1,0), stride = 1)

        self.conv_x5_conv1 = nn.Conv2d(input_depth,384,kernel_size = 1, stride = 1)
        self.conv_x5_conv2 = nn.Conv2d(384,448,kernel_size = (1,3), padding = (0,1), stride = 1)
        self.conv_x5_conv3 = nn.Conv2d(448,512,kernel_size = (3,1), padding = (1,0), stride = 1)
        self.conv_x5_conv4_1 = nn.Conv2d(512,256,kernel_size = (1,3), padding = (0,1), stride = 1)
        self.conv_x5_conv4_2 = nn.Conv2d(512,256,kernel_size = (3,1), padding = (1,0), stride = 1)

    def forward(self,x):

        out_average = self.pooling_average(x)
        out_average = self.pooling_conv(out_average)

        out_conv_x1 = self.conv_x1_conv(x)

        out_conv_x3 = self.conv_x3_conv1(x)
        out_conv_x3_1 = self.conv_x3_conv2_1(out_conv_x3)
        out_conv_x3_2 = self.conv_x3_conv2_2(out_conv_x3)
        
        out_conv_x5 = self.conv_x5_conv1(x)
        out_conv_x5 = self.conv_x5_conv2(out_conv_x5)
        out_conv_x5 = self.conv_x5_conv3(out_conv_x5)
        out_conv_x5_1 = self.conv_x5_conv4_1(out_conv_x5)
        out_conv_x5_2 = self.conv_x5_conv4_2(out_conv_x5)
        
        return torch.cat((out_average, out_conv_x1, out_conv_x3_1, out_conv_x3_2, out_conv_x5_1, out_conv_x5_2), 1)

class InceptionStem(nn.Module):
    def __init__(self):
        super(InceptionStem,self).__init__()

        #stage 1
        self.stage1_conv1 = nn.Conv2d(3,32,kernel_size = 3, stride = 2)
        self.stage1_conv2 = nn.Conv2d(32,32,kernel_size = 3)
        self.stage1_conv3 = nn.Conv2d(32,64,kernel_size = 3, padding = 1) # V?
        self.stage1_pool4_1 = nn.MaxPool2d(3, stride = 2)
        self.stage1_conv4_2 = nn.Conv2d(64,96,kernel_size = 3, stride = 2)

        #stage 2
        self.stage2_conv1_1 = nn.Conv2d(160,64,kernel_size = 3, padding = 1)
        self.stage2_conv2_1 = nn.Conv2d(64,96,kernel_size = 3) # V?

        self.stage2_conv1_2 = nn.Conv2d(160,64,kernel_size = 3, padding = 1)
        self.stage2_conv2_2 = nn.Conv2d(64,64,kernel_size = 3, padding = 1)
        self.stage2_conv3_2 = nn.Conv2d(64,64,kernel_size = 3, padding = 1)
        self.stage2_conv4_2 = nn.Conv2d(64,96,kernel_size = 3) # V?

        #stage 3
        self.stage3_conv = nn.Conv2d(192,192,kernel_size = 3, stride = 2)
        self.stage3_pool = nn.MaxPool2d(3, stride = 2) # V?

    def forward(self,x):

        #stage 1
        stage1_out = self.stage1_conv1(x)
        stage1_out = self.stage1_conv2(stage1_out)
        stage1_out = self.stage1_conv3(stage1_out)
        stage1_pool_out = self.stage1_pool4_1(stage1_out)
        stage1_conv_out = self.stage1_conv4_2(stage1_out)

        stage1_out = torch.cat((stage1_pool_out, stage1_conv_out), 1)

        #stage 2
        stage2_out1 = self.stage2_conv1_1(stage1_out)
        stage2_out1 = self.stage2_conv2_1(stage2_out1)

        stage2_out2 = self.stage2_conv1_2(stage1_out)
        stage2_out2 = self.stage2_conv2_2(stage2_out2)
        stage2_out2 = self.stage2_conv3_2(stage2_out2)
        stage2_out2 = self.stage2_conv4_2(stage2_out2)

        stage2_out = torch.cat((stage2_out1, stage2_out2), 1)

        #stage 3
        stage3_out_conv = self.stage3_conv(stage2_out)
        stage3_out_pool = self.stage3_pool(stage2_out)

        out = torch.cat((stage3_out_conv, stage3_out_pool), 1)

        return out

class InceptionReductionA(nn.Module):
    def __init__(self):
        super(InceptionReductionA,self).__init__()
    
    def forward(self,x):
        return x

class InceptionReductionB(nn.Module):
    def __init__(self):
        super(InceptionReductionB,self).__init__()
    
    def forward(self,x):
        return x

class InceptionV4(nn.Module):
    def __init__(self):
        super(InceptionV4,self).__init__()

        self.inception = nn.Sequential(
            InceptionStem(),
            InceptionBlockA(),
            InceptionBlockA(),
            InceptionBlockA(),
            InceptionBlockA(),
            InceptionReductionA(),
            InceptionBlockB(),
            InceptionBlockB(),
            InceptionBlockB(),
            InceptionBlockB(),
            InceptionBlockB(),
            InceptionBlockB(),
            InceptionBlockB(),
            InceptionReductionB(),
            InceptionBlockC(),
            InceptionBlockC(),
            InceptionBlockC(),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(1),
            nn.Dropout(0.8),
            nn.Softmax(1)
        )

    def forward(self,x):
        return self.inception(x)

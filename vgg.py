import torch
import torch.nn as nn
import numpy as np

class VGG16(nn.Module):
    def __init__(self, input_size = (224,224), num_class = 1000, classification = True):
        super(VGG16,self).__init__()

        self.conv_layers = nn.Sequential(
            #Block 1
            nn.Conv2d(3, 64, 3, padding = 1),
            nn.Conv2d(64, 64, 3, padding = 1),
            nn.MaxPool2d(2,2),
            nn.ReLU(),
            nn.BatchNorm2d(64),

            #Block 2
            nn.Conv2d(64, 128, 3, padding = 1),
            nn.Conv2d(128, 128, 3, padding = 1),
            nn.MaxPool2d(2,2),
            nn.ReLU(),
            nn.BatchNorm2d(128),

            #Block 3
            nn.Conv2d(128, 256, 3, padding = 1),
            nn.Conv2d(256, 256, 3, padding = 1),
            nn.Conv2d(256, 256, 3, padding = 1),
            nn.MaxPool2d(2,2),
            nn.ReLU(),
            nn.BatchNorm2d(256),

            #Block 4
            nn.Conv2d(256, 512, 3, padding = 1),
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.MaxPool2d(2,2),
            nn.ReLU(),
            nn.BatchNorm2d(512),

            #Block 5
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.MaxPool2d(2,2),
            nn.ReLU(),
            nn.BatchNorm2d(512)
        )

        self.classification = None
        if classification:
            w,h = input_size
            size = (w//32)*(h//32)*512
            self.classification = nn.Sequential(
                nn.Linear(size, 4096),
                nn.ReLU(),
                nn.Linear(4096, 4096),
                nn.ReLU(),
                nn.Linear(4096, num_class),
                nn.Softmax(dim = -1)
            )

    def forward(self,x, dense = True):

        out = self.conv_layers(x)

        if (self.classification is not None) and dense:
            batch_size,_,_,_=out.size()
            out = self.classification(out.view(batch_size,-1))

        return out

class VGG19(nn.Module):
    def __init__(self, input_size = (224,224), num_class = 1000, classification = True):
        super(VGG19,self).__init__()

        self.conv_layers = nn.Sequential(
            #Block 1
            nn.Conv2d(3, 64, 3, padding = 1),
            nn.Conv2d(64, 64, 3, padding = 1),
            nn.MaxPool2d(2,2),
            nn.ReLU(),
            nn.BatchNorm2d(64),

            #Block 2
            nn.Conv2d(64, 128, 3, padding = 1),
            nn.Conv2d(128, 128, 3, padding = 1),
            nn.MaxPool2d(2,2),
            nn.ReLU(),
            nn.BatchNorm2d(128),

            #Block 3
            nn.Conv2d(128, 256, 3, padding = 1),
            nn.Conv2d(256, 256, 3, padding = 1),
            nn.Conv2d(256, 256, 3, padding = 1),
            nn.Conv2d(256, 256, 3, padding = 1),
            nn.MaxPool2d(2,2),
            nn.ReLU(),
            nn.BatchNorm2d(256),

            #Block 4
            nn.Conv2d(256, 512, 3, padding = 1),
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.MaxPool2d(2,2),
            nn.ReLU(),
            nn.BatchNorm2d(512),

            #Block 5
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.Conv2d(512, 512, 3, padding = 1),
            nn.MaxPool2d(2,2),
            nn.ReLU(),
            nn.BatchNorm2d(512)
        )

        self.classification = None
        if classification:
            w,h = input_size
            size = (w//32)*(h//32)*512
            self.classification = nn.Sequential(
                nn.Linear(size, 4096),
                nn.ReLU(),
                nn.Linear(4096, 4096),
                nn.ReLU(),
                nn.Linear(4096, num_class),
                nn.Softmax(dim = -1)
            )

    def forward(self,x, dense = True):

        out = self.conv_layers(x)

        if (self.classification is not None) and dense:
            batch_size,_,_,_=out.size()
            out = self.classification(out.view(batch_size,-1))

        return out

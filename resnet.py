from typing import Union, Any, List, Optional, Tuple, Type, cast

import torch
import torch.nn as nn
import numpy as np

from tools import Scale

class ResNetBlock(nn.Module):
    def __init__(self, input_depth : int,
                        filter_count : int,
                        scale : Type[Scale] = Scale.SAME,
                        bottleneck : bool = False):
        super(ResNetBlock,self).__init__()

        output_depth = filter_count
        if bottleneck:
            output_depth *= 4

        if scale == Scale.SAME and input_depth != output_depth:
            self.scaling = nn.Sequential(
                nn.Conv2d(input_depth, output_depth, 1),
                nn.BatchNorm2d(output_depth)
            )
        elif scale == Scale.SAME:
            self.scaling = None
        elif scale == Scale.DOWNSCALE:
            self.scaling = nn.Sequential(
                nn.Conv2d(input_depth, output_depth, 1, stride = 2),
                nn.BatchNorm2d(output_depth)
            )
        elif scale == Scale.UPSCALE:
            self.scaling = nn.Sequential(
                nn.ConvTranspose2d(input_depth, output_depth, 1,
                                    stride = 2, output_padding = 1),
                nn.BatchNorm2d(output_depth)
            )

        if bottleneck:
            if scale == Scale.SAME:
                first_conv = nn.Conv2d(input_depth, filter_count, 1,
                                        bias = False, stride = 1)
            elif scale == Scale.DOWNSCALE:
                first_conv = nn.Conv2d(input_depth, filter_count, 1,
                                        bias = False, stride = 2)
            elif scale == Scale.UPSCALE:
                first_conv = nn.ConvTranspose2d(input_depth, filter_count, 1,
                                                bias = False, stride = 2,
                                                output_padding = 1)

            self.convolutions = nn.Sequential(
                first_conv,
                nn.BatchNorm2d(filter_count),
                nn.ReLU(),
                nn.Conv2d(filter_count, filter_count, 3, padding = 1),
                nn.BatchNorm2d(filter_count),
                nn.ReLU(),
                nn.Conv2d(filter_count, output_depth, 1, bias = False),
                nn.BatchNorm2d(output_depth)
            )
        else:
            if scale == Scale.SAME:
                first_conv = nn.Conv2d(input_depth, filter_count, 3,
                                        padding = 1, bias = False, stride = 1)
            elif scale == Scale.DOWNSCALE:
                first_conv = nn.Conv2d(input_depth, filter_count, 3,
                                        padding = 1, bias = False, stride = 2)
            elif scale == Scale.UPSCALE:
                first_conv = nn.ConvTranspose2d(input_depth, filter_count, 3,
                                                bias = False, stride = 2,
                                                padding = 1, output_padding = 1)
            
            self.convolutions = nn.Sequential(
                first_conv,
                nn.BatchNorm2d(filter_count),
                nn.ReLU(),
                nn.Conv2d(filter_count, output_depth, 3, padding = 1),
                nn.BatchNorm2d(output_depth)
            )
        
        self.activation = nn.ReLU()

    def forward(self,x):
        if self.scaling is None:
            #print(x.size(),self.convolutions(x).size())
            y = self.convolutions(x) + x
        else:
            #print(x.size(),self.convolutions(x).size(),self.scaling(x).size())
            y = self.convolutions(x) + self.scaling(x)
        return self.activation(y)

class ResNetEncoder(nn.Module):
    def __init__(self, blocks_count : List[int],
                        bottleneck : bool = False):
        super(ResNetEncoder,self).__init__()
        stages = []

        filter_size = 64
        stages.append(nn.Conv2d(3, filter_size, 7, stride = 2))
        stages.append(nn.BatchNorm2d(filter_size))
        stages.append(nn.ReLU())
        stages.append(nn.MaxPool2d(3, stride = 2))

        input_depth = filter_size
        for i,blocks in enumerate(blocks_count):
            stages.append(self.build_stage(block_count = blocks,
                                            input_depth = input_depth,
                                            filter_count = filter_size,
                                            bottleneck = bottleneck,
                                            downsample = (i != 0)))
            if bottleneck:
                input_depth = filter_size * 4
            else:
                input_depth = filter_size
            filter_size *= 2
        
        self.encoder = nn.Sequential(*stages)

    def build_stage(self, block_count : int,
                            input_depth : int,
                            filter_count : int,
                            bottleneck : bool = False,
                            downsample : bool = True):
        assert block_count > 0
        assert input_depth > 0
        assert filter_count > 0
                
        layers = []

        first_block_scale = Scale.SAME
        if downsample:
            first_block_scale = Scale.DOWNSCALE
        
        block_input_depth = filter_count
        if bottleneck:
            block_input_depth = filter_count * 4
        
        layers.append(ResNetBlock(input_depth = input_depth,
                                filter_count = filter_count,
                                scale = first_block_scale,
                                bottleneck = bottleneck))
        
        for _ in range(1,block_count):
            layers.append(ResNetBlock(input_depth = block_input_depth,
                                    filter_count = filter_count,
                                    scale = Scale.SAME,
                                    bottleneck = bottleneck))
        
        return nn.Sequential(*layers)

    def forward(self,x):
        return self.encoder(x)

class ResNetDecoder(nn.Module):
    def __init__(self, blocks_count : List[int],
                        bottleneck : bool = False):
        super(ResNetDecoder,self).__init__()
        stages = []

        filter_size = 512
        stages.append(nn.Conv2d(3, filter_size, 7, stride = 2))
        stages.append(nn.BatchNorm2d(filter_size))
        stages.append(nn.ReLU())
        stages.append(nn.MaxPool2d(3, stride = 2))

        input_depth = filter_size
        for i,blocks in enumerate(blocks_count):
            stages.append(self.build_stage(block_count = blocks,
                                            input_depth = input_depth,
                                            filter_count = filter_size,
                                            bottleneck = bottleneck,
                                            upsample = (i != 0)))
            if bottleneck:
                input_depth = filter_size * 4
            else:
                input_depth = filter_size
            filter_size //= 2
        
        stages.append(nn.AdaptiveAvgPool2d((1, 1)))
        stages.append(nn.Flatten(1))
        
        self.decoder = nn.Sequential(*stages)

    def build_stage(self, block_count : int,
                            input_depth : int,
                            filter_count : int,
                            bottleneck : bool = False,
                            upsample : bool = True):
        assert block_count > 0
        assert input_depth > 0
        assert filter_count > 0
                
        layers = []

        first_block_scale = Scale.SAME
        if upsample:
            first_block_scale = Scale.UPSCALE
        
        block_input_depth = filter_count
        if bottleneck:
            block_input_depth = filter_count * 4
        
        layers.append(ResNetBlock(input_depth = input_depth,
                                filter_count = filter_count,
                                scale = first_block_scale,
                                bottleneck = bottleneck))
        
        for _ in range(1,block_count):
            layers.append(ResNetBlock(input_depth = block_input_depth,
                                    filter_count = filter_count,
                                    scale = Scale.SAME,
                                    bottleneck = bottleneck))
        
        return nn.Sequential(*layers)

    def forward(self,x):
        return self.decoder(x)

class ResNet(nn.Module):
    def __init__(self, blocks_count : List[int],
                        class_count : int = 1000,
                        input_size : Tuple[int,int] = (224,244),
                        bottleneck : bool = False):
        super(ResNet,self).__init__()

        (w,h) = input_size

        encoder_output_depth = 64 * (1 << (len(blocks_count) - 1))
        if bottleneck:
            encoder_output_depth *= 4
        
        self.resnet = nn.Sequential(
            ResNetEncoder(blocks_count = blocks_count,
                            bottleneck = bottleneck),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(1),
            nn.Linear(encoder_output_depth, class_count),
            nn.Softmax(1)
        )
    
    def forward(self,x):
        return self.resnet(x)

def build_resnet18(input_size : Tuple[int,int] = (224,244),
                    class_count : int = 1000):
    layers = [2,2,2,2]
    return ResNet(blocks_count = layers,
                    bottleneck = False,
                    input_size = input_size,
                    class_count = class_count)

def build_resnet34(input_size : Tuple[int,int] = (224,244),
                    class_count : int = 1000):
    layers = [3,4,6,3]
    return ResNet(blocks_count = layers,
                    bottleneck = False,
                    input_size = input_size,
                    class_count = class_count)

def build_resnet50(input_size : Tuple[int,int] = (224,244),
                    class_count : int = 1000):
    layers = [3,4,6,3]
    return ResNet(blocks_count = layers,
                    bottleneck = True,
                    input_size = input_size,
                    class_count = class_count)

def build_resnet101(input_size : Tuple[int,int] = (224,244),
                    class_count : int = 1000):
    layers = [3,4,23,3]
    return ResNet(blocks_count = layers,
                    bottleneck = True,
                    input_size = input_size,
                    class_count = class_count)

def build_resnet152(input_size : Tuple[int,int] = (224,244),
                    class_count : int = 1000):
    layers = [3,8,36,3]
    return ResNet(blocks_count = layers,
                    bottleneck = True,
                    input_size = input_size,
                    class_count = class_count)

from enum import Enum

class Scale(Enum):
    UPSCALE = 1
    SAME = 0
    DOWNSCALE = -1

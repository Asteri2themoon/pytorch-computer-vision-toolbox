import torch
import torch.nn as nn

import numpy as np

#from basic_upscale import BasicUpscale
from utils import save_image
from dataset_celeba import get_loader
#from display import *
from srgan import Discriminator, Generator, train

import matplotlib.pyplot as plt

print('load dataset')
train_loader, test_loader = get_loader(32)

# SRGAN
dis = Discriminator(input_size = (128,128)).to('cuda')
gen = Generator(input_size = (128,128),residual_blocks = 16).to('cuda')

print('train')
train(train_loader, test_loader, gen, dis, tensorboard = True)

'''
gen = Generator(input_size = (128,128), residual_blocks = 16)
gen.load_state_dict(torch.load('results/srgan/gen.pth'))
gen.eval()
gen = gen.to('cuda')

with torch.no_grad():
    for x,_ in test_loader:
        x = x.to('cuda')
        x_tilde = gen(x)
        break


for i in range(16):
    save_image(x[i,:,:,:].cpu().clone(),'results/test/original{}.png'.format(i))
    save_image(x_tilde[i,:,:,:].cpu().clone(),'results/test/rescaled{}.png'.format(i))
'''
'''
print('load dataset')
train_loader, test_loader = get_loader(16)

print('train model')
model = BasicUpscale()
downsample = nn.AvgPool2d(4)

loss_fn = nn.MSELoss(reduction='mean')
learning_rate = 5e-7
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

print('optimize')
min_loss = 100
min_count = 0
early_stop = 50
losses = []

for i in range(500):
    for j,(x,_) in enumerate(train_loader):
        small_x = downsample(x)
        upsample_x = model(small_x)

        # Compute and print loss.
        loss = loss_fn(x, upsample_x)
        losses.append(loss.item())
        if j % 1 == 0:
            print('epoch: {} batch: {}/{} loss: {:.4f}'.format(i,j,len(train_loader),loss.item()))
        
        if loss < min_loss:
            min_loss = loss
            min_count = 0
        else:
            min_count += 1
        
        if early_stop < min_count:
            print('early stop')
            break

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    
    if early_stop < min_count:
        break

plt.plot(losses)
plt.title('deep decoder trianing')
plt.xlabel('batch')
plt.ylabel('loss')
plt.savefig('training.svg')

torch.save(model.state_dict(), 'model.pth')
torch.save(optimizer.state_dict(), 'optimizer.pth')

print('test')
def upscale_deepmodel(img,scale):
    model.eval()
    with torch.no_grad():
        w,h,d = img.shape
        img = (np.transpose(img,(2,0,1))/127.5)-1 # normalize
        img = torch.from_numpy(img).float().view((-1,d,w,h)) # reshape from numpy to torch
        result = model(img) # forward pass
        result = np.transpose(result.view((d,w*4,h*4)).numpy(),(1,2,0)) # reshape from torch to numpy
    return result*0.5+0.5 # denormalize

image = load_test_image(size = 512)
display(image,upscale_functions=[upscale_linear,upscale_cubic,upscale_deepmodel],upscale_label=['linear','cubic','deep model'])

for x,_ in test_loader:
    image = np.transpose(((x[0]+1)*127.5).numpy(),(1,2,0)).astype(np.uint8)
    break
    
display(image,upscale_functions=[upscale_linear,upscale_cubic,upscale_deepmodel],upscale_label=['linear','cubic','deep model'])
'''

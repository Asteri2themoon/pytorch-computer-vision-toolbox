import torch
import torch.nn as nn

class InceptionResNetBlockA(nn.Module):
    def __init__(self, version = 1):
        assert version == 1 or version == 2

        super(InceptionResNetBlockA,self).__init__()

        if version == 1:
            input_depth = 256
            conv_kernels_count = 32
            conv5_2_kernels_count = 32
            conv5_3_kernels_count = 32
        else:
            input_depth = 384
            conv_kernels_count = 32
            conv5_2_kernels_count = 48
            conv5_3_kernels_count = 64

        self.conv_x1_conv = nn.Conv2d(input_depth,conv_kernels_count,1, stride = 1)

        self.conv_x3_conv1 = nn.Conv2d(input_depth,conv_kernels_count,1, stride = 1)
        self.conv_x3_conv2 = nn.Conv2d(conv_kernels_count,conv_kernels_count,3, padding = 1, stride = 1)

        self.conv_x5_conv1 = nn.Conv2d(input_depth,conv_kernels_count,1, stride = 1)
        self.conv_x5_conv2 = nn.Conv2d(conv_kernels_count,conv5_2_kernels_count,3, padding = 1, stride = 1)
        self.conv_x5_conv3 = nn.Conv2d(conv5_2_kernels_count,conv5_3_kernels_count,3, padding = 1, stride = 1)

        self.conv_output = nn.Conv2d(conv_kernels_count*2+conv5_3_kernels_count,input_depth,1, stride = 1)

        self.activation = nn.ReLU()

    def forward(self,x):

        out_conv_x1 = self.conv_x1_conv(x)

        out_conv_x3 = self.conv_x3_conv1(x)
        out_conv_x3 = self.conv_x3_conv2(out_conv_x3)

        out_conv_x5 = self.conv_x5_conv1(x)
        out_conv_x5 = self.conv_x5_conv2(out_conv_x5)
        out_conv_x5 = self.conv_x5_conv3(out_conv_x5)

        out_concat = torch.cat((out_conv_x1, out_conv_x3, out_conv_x5), 1)

        out = self.conv_output(out_concat)
        out = self.activation(x + out)

        return out

class InceptionResNetBlockB(nn.Module):
    def __init__(self, version = 1):
        assert version == 1 or version == 2

        super(InceptionResNetBlockB,self).__init__()

        if version == 1:
            input_depth = 896
            conv_kernels_count = 128
            conv7_1_kernels_count = 128
            conv7_2_kernels_count = 128
        else:
            input_depth = 1154
            conv_kernels_count = 192
            conv7_1_kernels_count = 128
            conv7_2_kernels_count = 160

        self.conv_x1_conv = nn.Conv2d(input_depth, conv_kernels_count, 1, stride = 1)

        self.conv_x7_conv1 = nn.Conv2d(input_depth, conv7_1_kernels_count, 1, stride = 1)
        self.conv_x7_conv2 = nn.Conv2d(conv7_1_kernels_count, conv7_2_kernels_count,
                                        kernel_size = (1,7), padding = (0,3), stride = 1)
        self.conv_x7_conv3 = nn.Conv2d(conv7_2_kernels_count, conv_kernels_count,
                                        kernel_size = (7,1), padding = (3,0), stride = 1)

        self.conv_output = nn.Conv2d(conv_kernels_count*2, input_depth, 1, stride = 1)

        self.activation = nn.ReLU()

    def forward(self,x):

        out_conv_x1 = self.conv_x1_conv(x)

        out_conv_x7 = self.conv_x7_conv1(x)
        out_conv_x7 = self.conv_x7_conv2(out_conv_x7)
        out_conv_x7 = self.conv_x7_conv3(out_conv_x7)

        out_concat = torch.cat((out_conv_x1, out_conv_x7), 1)

        out = self.conv_output(out_concat)
        out = self.activation(x + out)

        return out

class InceptionResNetBlockC(nn.Module):
    def __init__(self, version = 1):
        assert version == 1 or version == 2

        super(InceptionResNetBlockC,self).__init__()

        if version == 1:
            input_depth = 1792
            conv_kernels_count = 192
            conv3_2_kernels_count = 192
            conv3_3_kernels_count = 192
        else:
            input_depth = 2048
            conv_kernels_count = 192
            conv3_2_kernels_count = 224
            conv3_3_kernels_count = 256

        self.conv_x1_conv = nn.Conv2d(input_depth, conv_kernels_count, 1, stride = 1)

        self.conv_x3_conv1 = nn.Conv2d(input_depth, conv_kernels_count, 1, stride = 1)
        self.conv_x3_conv2 = nn.Conv2d(conv_kernels_count, conv3_2_kernels_count, 
                                        kernel_size = (1,3), padding = (0,1), stride = 1)
        self.conv_x3_conv3 = nn.Conv2d(conv3_2_kernels_count, conv3_3_kernels_count,
                                        kernel_size = (3,1), padding = (1,0), stride = 1)

        self.conv_output = nn.Conv2d(conv_kernels_count+conv3_3_kernels_count, input_depth, 1, stride = 1)

        self.activation = nn.ReLU()

    def forward(self,x):

        out_conv_x1 = self.conv_x1_conv(x)

        out_conv_x3 = self.conv_x3_conv1(x)
        out_conv_x3 = self.conv_x3_conv2(out_conv_x3)
        out_conv_x3 = self.conv_x3_conv3(out_conv_x3)

        out_concat = torch.cat((out_conv_x1, out_conv_x3), 1)

        out = self.conv_output(out_concat)
        out = self.activation(x + out)

        return out

class InceptionResNetV4(nn.Module):
    def __init__(self):
        super(InceptionResNetV4,self).__init__()

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.distributions.normal import Normal
import torch.nn.functional as F

import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import time
import json
import os
import glob
import uuid

import telegram_send

from dataset_celeba import get_loader
from utils import save_image
from stylegan import Generator, Discriminator

stop = False

def plt_init():
    plt.ion()
    plt.show()
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())

def get_step(path):
    file_name = os.path.basename(path) 
    file_name, _ = os.path.splitext(file_name)
    [base, stage, epoch] = file_name.split('_')
    return int(stage), int(epoch)

def get_last_checkpoint(directory = './results'):
    expr = os.path.join(directory,'stylegan*_*_*.pth')

    try:
        checkpoints = [get_step(f) for f in glob.glob(expr)]
        lower = checkpoints[0]
    except:
        return None
    
    for checkpoint in checkpoints[1:]:
        c_stage, c_epoch = checkpoint
        l_stage, l_epoch = lower
        if c_stage > l_stage:
            lower = checkpoint
        elif c_stage == l_stage and c_epoch > l_epoch:
            lower = checkpoint
    
    return glob.glob(os.path.join(directory,'stylegan*_{}_{}.pth'.format(*lower)))[0]

def save_losses(gen_losses, dis_losses, stage, file_name, parameters, x, x_tilde, n_max = 1000):
    parameters = parameters.copy()
    gen_losses = np.array(gen_losses)
    dis_losses = np.array(dis_losses)

    batch_size = x.shape[0]

    def stop_training(event):
        global stop

        if not stop:
            try:
                if event.key == 'q':
                    print('stop training')
                    stop = True
            except AttributeError:
                print('stop training')
                stop = True
    
    fig = plt.gcf()
    fig.canvas.mpl_disconnect('close_event')
    fig.canvas.mpl_disconnect('key_press_event')

    plt.clf()

    plt.gcf().canvas.mpl_connect('close_event', stop_training)
    plt.gcf().canvas.mpl_connect('key_press_event', stop_training)

    #prop_cycle = plt.rcParams['axes.prop_cycle']
    #colors = prop_cycle.by_key()['color']
    stages = parameters['stages']

    lr_gen = stages[stage]['lr_gen']
    lr_dis = stages[stage]['lr_dis']
    res_x, res_y = parameters['const_image_size']
    res_x <<= stage
    res_y <<= stage

    title = 'Style GAN\nstage: {} resolution: {}x{} lr_gen: {}, lr_dis: {}'
    plt.suptitle(title.format(stage, res_x, res_y, lr_gen, lr_dis))

    plt.subplot2grid((4, batch_size), (0, 0), rowspan=2, colspan=batch_size//2)
    plt.subplots_adjust(top = 0.85)
    plt.title('training')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.yscale('log')

    plt.plot(gen_losses, label='generator {:.4f}'.format(gen_losses[-1]))
    plt.plot(dis_losses, label='discriminator {:.4f}'.format(dis_losses[-1]))
    plt.legend()

    
    plt.subplot2grid((4, batch_size), (2, 0), rowspan=2, colspan=batch_size//2)
    plt.subplots_adjust(top = 0.85)
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.yscale('log')

    batch = np.arange(0, gen_losses.shape[0], step = 1)
    if gen_losses.shape[0] > n_max:
        batch = batch[-n_max:]
        gen_losses = gen_losses[-n_max:]
        dis_losses = dis_losses[-n_max:]
    
    plt.plot(batch, gen_losses, label='generator'.format(gen_losses[-1]))
    plt.plot(batch, dis_losses, label='discriminator'.format(dis_losses[-1]))
    plt.legend()
    

    for i in range(batch_size//2):
        plt.subplot2grid((4, batch_size), (0, batch_size//2+i))
        plt.subplots_adjust(top = 0.85)
        plt.title('generated image {}'.format(i))
        plt.axis('off')
        plt.imshow(np.transpose(x_tilde[i,:,:,:].cpu().clone().detach(),(1,2,0))*0.5+0.5)
        plt.subplot2grid((4, batch_size), (2, batch_size//2+i))
        plt.subplots_adjust(top = 0.85)
        plt.title('real image {}'.format(i))
        plt.axis('off')
        plt.imshow(np.transpose(x[i,:,:,:].cpu().clone().detach(),(1,2,0))*0.5+0.5)

        plt.subplot2grid((4, batch_size), (1, batch_size//2+i))
        plt.subplots_adjust(top = 0.85)
        plt.title('generated image {}'.format(i+batch_size//2))
        plt.axis('off')
        plt.imshow(np.transpose(x_tilde[i+batch_size//2,:,:,:].cpu().clone().detach(),(1,2,0))*0.5+0.5)
        plt.subplot2grid((4, batch_size), (3, batch_size//2+i))
        plt.subplots_adjust(top = 0.85)
        plt.title('real image {}'.format(i+batch_size//2))
        plt.axis('off')
        plt.imshow(np.transpose(x[i+batch_size//2,:,:,:].cpu().clone().detach(),(1,2,0))*0.5+0.5)

    #plt.show()
    plt.savefig(file_name)

def resample(x, level, n):
    return F.avg_pool2d(x, kernel_size = 1<<(n-level))

def train(train_loader, test_loader, parameters, n, device, start_from = None, training_id = None):
    global stop

    const_image_size = parameters['const_image_size']
    latent_space_size = parameters['latent_space_size']
    feature_size = parameters['feature_size']
    fc_layer = parameters['fc_layer']
    lr_gen_discount = parameters['lr_gen_discount']

    stages = parameters['stages']

    for x,_ in train_loader:
        batch_size = x.shape[0]
        break
    
    gen = Generator(const_image_size = const_image_size,
                    latent_space_size = latent_space_size,
                    feature_size = feature_size,
                    fc_layer = fc_layer,
                    block_count = len(stages)).to(device)
    
    dis = Discriminator(const_image_size = const_image_size,
                        feature_size = feature_size,
                        block_count = len(stages)).to(device)

    #print([p[0] for p in gen.named_parameters()])

    loss = nn.BCELoss().to(device)
    valid = Variable(torch.ones((batch_size,1)), requires_grad=False).to(device)
    fake = Variable(torch.zeros((batch_size,1)), requires_grad=False).to(device)
    
    gen_losses = []
    dis_losses = []

    stage0, epoch0 = 0, 0
    if start_from is not None:
        checkpoint = torch.load(start_from)

        gen.load_state_dict(checkpoint['model_gen'])
        dis.load_state_dict(checkpoint['model_dis'])

        gen_losses = checkpoint['loss_gen']
        dis_losses = checkpoint['loss_dis']

        stage0, epoch0 = get_step(start_from)
        epoch_count = stages[stage0]['epochs']
        if epoch_count <= (epoch0+1):
            stage0 += 1
            epoch0 = 0
        else:
            epoch0 += 1

    batch_count = len(train_loader)
    for stage, epoch in enumerate(stages[stage0:], stage0):
        epoch_count = epoch['epochs']
        lr_gen = epoch['lr_gen']
        lr_dis = epoch['lr_dis']
        blend_alpha = epoch['blend_alpha']

        gen.setStage(stage)
        dis.setStage(stage)

        gen_settings = gen.optimizer_setup(lr_gen, lr_gen_discount)
        optimizer_gen = torch.optim.Adam(gen_settings, lr = lr_gen)
        #optimizer_gen = torch.optim.Adam(gen.parameters(), lr = lr_gen)
        optimizer_dis = torch.optim.Adam(dis.parameters(), lr = lr_dis)
        
        for i in range(epoch0, epoch_count):
            for j,(x,_) in enumerate(train_loader):
                t0 = time.time()
                
                alpha = 1.0
                if blend_alpha > 0:
                    alpha = (j+i*batch_count)/(batch_count*blend_alpha)
                    if alpha > 1.0:
                        alpha = 1.0

                    gen.setAlpha(alpha)
                    dis.setAlpha(alpha)

                x = x.to(device)
                x = resample(x, stage, n)
                z = torch.randn((batch_size, latent_space_size), device = device)

                gen.train()
                dis.eval()
                optimizer_gen.zero_grad()

                x_tilde = gen(z)
                det = dis(x_tilde)
                loss_gen = loss(det,valid)

                loss_gen.backward()
                optimizer_gen.step()
                gen_losses.append(loss_gen.item())

                x_tilde = x_tilde.detach()
                
                gen.eval()
                dis.train()
                optimizer_dis.zero_grad()

                loss_dis = 1/2*(loss(dis(x_tilde),fake) + loss(dis(x),valid))

                if len(dis_losses) == 0 or dis_losses[-1] > 0.3:
                    loss_dis.backward()
                    optimizer_dis.step()

                dis_losses.append(loss_dis.item())

                res_x, res_y = const_image_size
                print('Loss epoch:{} ({}/{}) stage:{} resolution:{}x{} alpha:{:.3f} dis:{:.4f} gen:{:.4f} ({:.3f} sec)'.format(
                        i, j, batch_count, stage, res_x<<stage, res_y<<stage,
                        alpha, dis_losses[-1], gen_losses[-1], time.time()-t0))
                if (j%1000)==0:
                    file_name = 'results/stylegan.svg'
                    if training_id is not None:
                        file_name = 'results/stylegan{}.svg'.format(training_id)
                    
                    save_losses(gen_losses,
                                dis_losses,
                                stage,
                                file_name,
                                parameters,
                                x[:8],
                                x_tilde[:8],
                                n_max = epoch_count)
                    plt.pause(0.0001)

                if stop:
                    exit(0)

            file_name = 'results/tmp_telegram.png'
            save_losses(gen_losses,
                        dis_losses,
                        stage,
                        file_name,
                        parameters,
                        x[:8],
                        x_tilde[:8],
                        n_max = epoch_count)
            try:
                with open(file_name,'rb') as f:
                    telegram_send.send(images=[f])
            except:
                print('can\'t send image to telegram')

            file_name = 'results/stylegan_{}_{}.pth'.format(stage, i)
            if training_id is not None:
                file_name = 'results/stylegan{}_{}_{}.pth'.format(training_id, stage, i)
            torch.save({
                'model_gen': gen.state_dict(),
                'model_dis': dis.state_dict(),
                'loss_gen': gen_losses,
                'loss_dis': dis_losses,
                'optimizer_gen': optimizer_gen.state_dict(),
                'optimizer_dis': optimizer_dis.state_dict(),
                'parameters': parameters,
                }, file_name)

if __name__ == '__main__':

    train_loader, test_loader = get_loader(batch_size = 32)
    
    parameters = [{
        'const_image_size' : (4, 4),
        'latent_space_size' : 512,
        'feature_size' : 512,
        'fc_layer' : 8,

        'lr_gen_discount' : 1e-3,
        
        'stages': [
            {
                'epochs': 10,
                'blend_alpha': 0,
                'lr_gen' : 1e-5,
                'lr_dis' : 1e-5
            },
            {
                'epochs': 100,
                'blend_alpha': 20,
                'lr_gen' : 1e-5,
                'lr_dis' : 1e-5
            },
            {
                'epochs': 1000,
                'blend_alpha': 20,
                'lr_gen' : 1e-5,
                'lr_dis' : 1e-5
            },
        ]
    }]

    checkpoint = get_last_checkpoint()

    plt_init()

    for p in parameters:
        random_id = uuid.uuid4()

        str_parameters = json.dumps(p, indent=4, sort_keys=True)
        telegram_send.send(messages=['*Start training `{}` with parameters:*\n```\n{}\n```'.format(random_id, str_parameters)],
                        parse_mode = 'markdown')
        if checkpoint is not None:
            stage, epoch = get_step(checkpoint)
            telegram_send.send(messages=['Start from checkpoint ` {} `'.format(checkpoint)],
                            parse_mode = 'markdown')
        
        train(train_loader,
                test_loader,
                p, 5, 'cuda', 
                start_from = checkpoint, 
                training_id = random_id)
    

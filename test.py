import unittest
import torch

def test_block_constant_size(test, model,size):
	x = torch.ones(size)

	x2 = model.forward(x)
	y = model.forward(x2)

	if size != x.size():
		test.assertTrue(False)
	if size != x2.size():
		test.assertTrue(False)
	if size != y.size():
		test.assertTrue(False)

def test_block(test, model,input_size,output_size):
	x = torch.ones(input_size)
	y = model.forward(x)

	if input_size != x.size():
		test.assertTrue(False)
	if output_size != y.size():
		test.assertTrue(False)

class InceptionTest(unittest.TestCase):
	def test_inception(self):
		from inception import InceptionBlockA,InceptionBlockB,InceptionBlockC, InceptionStem

		#inception block A
		test_block_constant_size(self,
								InceptionBlockA(),
								(1,384,128,128))

		#inception block B
		test_block_constant_size(self,
								InceptionBlockB(),
								(1,1024,128,128))
		
		#inception block C
		test_block_constant_size(self,
								InceptionBlockC(),
								(1,1536,128,128))
		
		test_block(self,
					InceptionStem(),
					input_size = (1,3,299,299),
					output_size = (1,384,35,35))
		
		self.assertTrue(True)
	
	def test_inception_resnet(self):
		from inception_resnet import InceptionResNetBlockA,InceptionResNetBlockB,InceptionResNetBlockC

		#inception resnet block A
		test_block_constant_size(self,
								InceptionResNetBlockA(version=1),
								(1,256,128,128))

		test_block_constant_size(self,
								InceptionResNetBlockA(version=2),
								(1,384,128,128))

		#inception resnet block B
		test_block_constant_size(self,
								InceptionResNetBlockB(version=1),
								(1,896,128,128))

		test_block_constant_size(self,
								InceptionResNetBlockB(version=2),
								(1,1154,128,128))

		#inception resnet block C
		test_block_constant_size(self,
								InceptionResNetBlockC(version=1),
								(1,1792,128,128))

		test_block_constant_size(self,
								InceptionResNetBlockC(version=2),
								(1,2048,128,128))
		
		self.assertTrue(True)

	def test_resnet(self):
		from tools import Scale
		from resnet import ResNetBlock
		from resnet import build_resnet18, build_resnet34, build_resnet50, build_resnet101, build_resnet152
		
		size = 32
		w = h = 16
		
		test_block(self,
					ResNetBlock(input_depth = size,
								filter_count = size,
								scale = Scale.SAME,
								bottleneck = False),
					input_size = (1,size,w,h),
					output_size = (1,size,w,h))
		
		test_block(self,
					ResNetBlock(input_depth = size*4,
								filter_count = size,
								scale = Scale.SAME,
								bottleneck = True),
					input_size = (1,size*4,w,h),
					output_size = (1,size*4,w,h))
		
		test_block(self,
					ResNetBlock(input_depth = size,
								filter_count = size,
								scale = Scale.UPSCALE,
								bottleneck = False),
					input_size = (1,size,w,h),
					output_size = (1,size,w*2,h*2))
		
		test_block(self,
					ResNetBlock(input_depth = size,
								filter_count = size//2,
								scale = Scale.UPSCALE,
								bottleneck = False),
					input_size = (1,size,w,h),
					output_size = (1,size//2,w*2,h*2))
		
		test_block(self,
					ResNetBlock(input_depth = size*4,
								filter_count = size,
								scale = Scale.UPSCALE,
								bottleneck = True),
					input_size = (1,size*4,w,h),
					output_size = (1,size*4,w*2,h*2))
		
		test_block(self,
					ResNetBlock(input_depth = size*4,
								filter_count = size//2,
								scale = Scale.UPSCALE,
								bottleneck = True),
					input_size = (1,size*4,w,h),
					output_size = (1,size*2,w*2,h*2))
		
		test_block(self,
					ResNetBlock(input_depth = size,
								filter_count = size,
								scale = Scale.DOWNSCALE,
								bottleneck = False),
					input_size = (1,size,w,h),
					output_size = (1,size,w//2,h//2))
		
		test_block(self,
					ResNetBlock(input_depth = size,
								filter_count = size//2,
								scale = Scale.DOWNSCALE,
								bottleneck = False),
					input_size = (1,size,w,h),
					output_size = (1,size//2,w//2,h//2))
		
		test_block(self,
					ResNetBlock(input_depth = size*4,
								filter_count = size,
								scale = Scale.DOWNSCALE,
								bottleneck = True),
					input_size = (1,size*4,w,h),
					output_size = (1,size*4,w//2,h//2))
		
		test_block(self,
					ResNetBlock(input_depth = size*4,
								filter_count = size//2,
								scale = Scale.DOWNSCALE,
								bottleneck = True),
					input_size = (1,size*4,w,h),
					output_size = (1,size*2,w//2,h//2))
		
		test_block(self, build_resnet18(),
					input_size = (1,3,224,224),
					output_size = (1,1000))
		
		test_block(self, build_resnet34(),
					input_size = (1,3,224,224),
					output_size = (1,1000))
		
		test_block(self, build_resnet50(),
					input_size = (1,3,224,224),
					output_size = (1,1000))
		
		test_block(self, build_resnet101(),
					input_size = (1,3,224,224),
					output_size = (1,1000))
		
		test_block(self, build_resnet152(),
					input_size = (1,3,224,224),
					output_size = (1,1000))
		
		self.assertTrue(True)

	def test_vgg(self):
		from vgg import VGG16, VGG19

		w = h = 224
		num_class = 10
		
		#encoder
		test_block(self,
					VGG16(num_class = num_class,
						classification = False),
					input_size = (4,3,w,h),
					output_size = (4,512,w//32,h//32))
			
		test_block(self,
					VGG19(num_class = num_class,
						classification = False),
					input_size = (4,3,w,h),
					output_size = (4,512,w//32,h//32))
		
		#classifiaction
		test_block(self,
					VGG16(num_class = num_class,
						classification = True),
					input_size = (4,3,w,h),
					output_size = (4,num_class))
			
		test_block(self,
					VGG19(num_class = num_class,
						classification = True),
					input_size = (4,3,w,h),
					output_size = (4,num_class))
		
		self.assertTrue(True)

if __name__ == '__main__': 
	unittest.main() 

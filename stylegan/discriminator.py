import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.distributions.normal import Normal

class DisBlock(nn.Module):
    def __init__(self,
                feature_size = 512,
                alpha = 1):
        super(DisBlock, self).__init__()

        self.setAlpha(alpha)

        self.conv = nn.Sequential(
            nn.Conv2d(feature_size, feature_size, 3, padding = 1, bias = True),
            nn.PReLU(),
            nn.Conv2d(feature_size, feature_size, 3, padding = 1, bias = True),
            nn.PReLU()
        )
        self.downscale = nn.AvgPool2d(2)

    def setAlpha(self, alpha):
        assert 0 <= alpha <= 1

        self.alpha = alpha

    def forward(self, x):
        x_conv = self.conv(x)
        x_conv = self.downscale(x_conv)

        if self.alpha < 1:
            x_downsample = self.downscale(x)
            x_conv = self.alpha * x_conv + (1 - self.alpha) * x_downsample
        
        return x_conv

class FinalDisBlock(nn.Module):
    def __init__(self,
                image_size = (4, 4),
                feature_size = 512):
        super(FinalDisBlock, self).__init__()

        w, h = image_size

        self.net = nn.Sequential(
            nn.Conv2d(feature_size, feature_size, 3, padding = 1, bias = True),
            nn.AvgPool2d(image_size),
            nn.PReLU(),
            nn.Flatten(),
            nn.Linear(feature_size, 1),
            nn.Sigmoid()
        )

    def forward(self, x):
        return self.net(x)
    
class Discriminator(nn.Module):
    def __init__(self,
                const_image_size = (4, 4),
                feature_size = 512,
                block_count = 0,
                stage = 0):
        super(Discriminator, self).__init__()
        assert stage < block_count

        self.stage = stage
        self.alpha = 1

        self.const_image_size = const_image_size
        self.feature_size = feature_size

        self.from_rgb = nn.Conv2d(3, feature_size, 1) 

        self.final_block = FinalDisBlock(image_size = const_image_size,
                                        feature_size = feature_size)
        
        self.dis_blocks = []
        for i in range(block_count):
            self.dis_blocks.append(DisBlock(feature_size = feature_size))
        self.blocks = nn.ModuleList(self.dis_blocks)
        
    def setAlpha(self, alpha):
        assert self.stage < len(self.dis_blocks)
        assert 0 <= alpha <= 1.0

        self.alpha = alpha

        if len(self.dis_blocks) > 0:
            self.dis_blocks[-1-self.stage].setAlpha(self.alpha)
    
    def setStage(self, stage, alpha = 1.0):
        assert stage < len(self.dis_blocks)

        self.stage = stage
        
        for block in self.dis_blocks:
            block.setAlpha(1.0)

        if alpha < 1.0:
            self.dis_blocks[-self.stage].setAlpha(alpha)
    
    def forward(self, x):
        x = self.from_rgb(x)

        for block in self.dis_blocks[-1:-1-self.stage:-1]:
            x = block(x)
        
        x = self.final_block(x)

        return x

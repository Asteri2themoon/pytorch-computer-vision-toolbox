import torch
import torch.nn as nn
import torch.nn.init as init
import torch.nn.functional as F
from torch.autograd import Variable, Function
from torch.distributions.normal import Normal
import torch.nn.functional as forward

def init_linear(linear, bias = 0):
    #init.xavier_normal_(linear.weight)
    linear.weight.data.normal_()
    linear.bias.data[:] = bias

def init_conv(conv, glu=True, bias = 0):
    init.kaiming_normal_(conv.weight)
    conv.weight.data *= 0.001
    if conv.bias is not None:
        conv.bias.data[:] = bias

class PixelNorm(nn.Module):
    def __init__(self, epsilon=1e-8):
        super(PixelNorm, self).__init__()
        self.epsilon = epsilon

    def forward(self, x):
        tmp  = torch.mul(x, x) # or x ** 2
        tmp1 = torch.rsqrt(torch.mean(tmp, dim=1, keepdim=True) + self.epsilon)

        return x * tmp1

class FC(nn.Module):
    def __init__(self,
                latent_space_size = 512,
                gain = 2**0.5):
        super(FC,self).__init__()

        # he init
        he_std = gain * latent_space_size ** (-0.5)
        #init_std = 1.0 / lr

        self.weight = nn.Parameter(torch.randn(latent_space_size,
                                                latent_space_size) * he_std)
        self.bias = nn.Parameter(torch.zeros(latent_space_size))

    def forward(self, x):
        linear = F.linear(x, self.weight, self.bias)
        out = F.leaky_relu(linear, 0.2, inplace = True)
        return out

class MappingNetwork(nn.Module):
    def __init__(self,
                latent_space_size = 512,
                fc_layer = 8):
        super(MappingNetwork,self).__init__()

        fc = []
        for i in range(fc_layer):
            '''
            linear = nn.Linear(latent_space_size,
                                latent_space_size,
                                bias = True)
            init_linear(linear)
            fc.append(linear)
            fc.append(nn.LeakyReLU(0.2))
            '''
            fc.append(FC(latent_space_size = latent_space_size))

        self.fc = nn.Sequential(*fc)
    
    def forward(self, z):
        z = z / torch.sqrt(torch.mean(z**2, dim = 1, keepdim = True) + 1e-8)

        w = self.fc(z)
        return w

class A(nn.Module):
    def __init__(self,
                latent_space_size = 512,
                feature_size = 512):
        super(A,self).__init__()
        
        self.transform_mu = nn.Linear(latent_space_size,
                                    feature_size,
                                    bias = True)
        init_linear(self.transform_mu, bias = 0)
        self.transform_sigma = nn.Linear(latent_space_size,
                                    feature_size,
                                    bias = True)
        init_linear(self.transform_sigma, bias = 1)
    
    def forward(self, w):
        y_mu = self.transform_mu(w)
        y_sigma = self.transform_sigma(w)
        return y_mu, y_sigma

class B(nn.Module):
    def __init__(self,
                image_size = (4, 4),
                feature_size = 512):
        super(B,self).__init__()

        self.image_size = image_size
        self.feature_size = feature_size

        self.scale = nn.Parameter(torch.randn(feature_size,),
                                requires_grad=True)
    
    def forward(self, batch_size = 1):
        w, h = self.image_size

        noise = torch.randn((batch_size, self.feature_size, w * h),
                            device = self.scale.device)
        
        scaled = noise * self.scale[None, :, None]

        return scaled.view(batch_size, self.feature_size, *self.image_size)

class AdaIN(nn.Module):
    def __init__(self):
        super(AdaIN,self).__init__()
        self.pixel_norm = PixelNorm()
    
    def forward(self, x, y_mu, y_sigma):
        x = self.pixel_norm(x)

        batch_size = x.shape[0]
        feature_size = x.shape[1]
        w, h = x.shape[2], x.shape[3]

        x = x.view(-1, w * h)
        y_mu = y_mu.view(-1)
        y_sigma = y_sigma.view(-1)

        x_mu = torch.mean(x, dim = 1)
        x_sigma = torch.std(x, dim = 1)

        adain = y_sigma[:, None]*(x - x_mu[:, None])/(x_sigma[:, None]+1e-8)+y_mu[:, None]
        
        return adain.view(batch_size, feature_size, w, h)

class Const(nn.Module):
    def __init__(self,
                image_size = (4, 4),
                feature_size = 512):
        super(Const,self).__init__()

        w,h = image_size
        self.const = nn.Parameter(torch.ones(feature_size, w, h),
                                requires_grad=True)

    def forward(self, batch_size = 1):
        return self.const.repeat(batch_size,1,1,1)

class InitialGenBlock(nn.Module):
    def __init__(self,
                image_size = (4, 4),
                latent_space_size = 512,
                feature_size = 512):
        super(InitialGenBlock,self).__init__()

        self.adain = AdaIN()
        
        self.const = Const(image_size, feature_size)        
        self.b1 = B(image_size, feature_size)
        self.a1 = A(latent_space_size, feature_size)
        
        self.conv = nn.Conv2d(feature_size, feature_size, 3, padding = 1)
        init_conv(self.conv)

        self.lrelu = nn.LeakyReLU(0.2)

        self.b2 = B(image_size, feature_size)
        self.a2 = A(latent_space_size, feature_size)

    def forward(self, w):
        '''
        import matplotlib.pyplot as plt
        import numpy as np

        plt.ion()
        plt.clf()
        rgb = conv(x)
        plt.subplot(171)
        plt.title('const')
        plt.axis('off')
        plt.imshow(np.transpose(rgb[0,:3,:,:].cpu().clone().detach(),(1,2,0))*0.5+0.5)
        '''
        batch_size = w.shape[0]

        x = self.const(batch_size = batch_size)

        x = x + self.b1(batch_size = batch_size)
        y_mu, y_sigma = self.a1(w)
        x = self.adain(x, y_mu, y_sigma)
        x = self.lrelu(x)
        
        x = self.conv(x)
        
        x = x + self.b2(batch_size = batch_size)
        y_mu, y_sigma = self.a2(w)
        x = self.adain(x, y_mu, y_sigma)
        
        x = self.lrelu(x)

        return x

class GenBlock(nn.Module):
    def __init__(self,
                image_size = (8, 8),
                latent_space_size = 512,
                feature_size = 512):
        super(GenBlock,self).__init__()

        self.upsample = nn.Upsample(image_size,
                                    mode = 'bilinear',
                                    align_corners = False)

        self.adain = AdaIN()
        
        conv = nn.Conv2d(feature_size, feature_size, 3, padding = 1)
        init_conv(conv)
        self.conv1 = nn.Sequential(
            conv,
            nn.LeakyReLU(0.2)
        )
        self.b1 = B(image_size, feature_size)
        self.a1 = A(latent_space_size, feature_size)
        
        conv = nn.Conv2d(feature_size, feature_size, 3, padding = 1)    
        init_conv(conv)
        self.conv2 = nn.Sequential(
            conv,
            nn.LeakyReLU(0.2)
        )
        self.b2 = B(image_size, feature_size)
        self.a2 = A(latent_space_size, feature_size)

    def forward(self, x, w):
        assert x.shape[0] == w.shape[0]
        batch_size = x.shape[0]

        x_upsample = self.upsample(x)

        x = self.conv1(x_upsample)
        
        x = x + self.b1(batch_size = batch_size)
        y_mu, y_sigma = self.a1(w)
        x = self.adain(x, y_mu, y_sigma)

        x = self.conv2(x)

        x = x + self.b2(batch_size = batch_size)
        y_mu, y_sigma = self.a2(w)
        x = self.adain(x, y_mu, y_sigma)

        return x, x_upsample

class Generator(nn.Module):
    def __init__(self,
                const_image_size = (4, 4),
                latent_space_size = 512,
                feature_size = 512,
                fc_layer = 8,
                block_count = 0,
                stage = 0):
        super(Generator, self).__init__()
        assert stage <= block_count

        self.stage = stage
        self.alpha = 1

        self.const_image_size = const_image_size
        self.latent_space_size = latent_space_size
        self.feature_size = feature_size

        self.mapping_network = MappingNetwork(latent_space_size = latent_space_size,
                                            fc_layer = fc_layer)
        
        self.initial_block = InitialGenBlock(image_size = const_image_size,
                                        latent_space_size = latent_space_size,
                                        feature_size = feature_size)
        
        w, h = const_image_size
        self.gen_blocks = []
        for i in range(block_count):
            block = GenBlock(image_size = (w<<(i+1), h<<(i+1)),
                            latent_space_size = latent_space_size,
                            feature_size = feature_size)
            self.gen_blocks.append(block)
        
        self.blocks = nn.ModuleList(self.gen_blocks)

        conv = nn.Conv2d(feature_size, 3, 1)
        init_conv(conv)
        self.to_rgb = nn.Sequential(
            conv,
            nn.Tanh()
        )

    def setAlpha(self, alpha):
        assert 0 <= alpha <= 1

        self.alpha = alpha
    
    def setStage(self, stage, alpha = 1.0):
        assert stage < len(self.gen_blocks)

        self.stage = stage
    
    def forward(self, z):
        w = self.mapping_network(z)
        
        x = self.initial_block(w)

        for block in self.gen_blocks[:self.stage]:
            x, upsample = block(x, w)
        
        if self.alpha < 1:
            x = x * self.alpha + upsample * (1 - self.alpha)
        
        x = self.to_rgb(x)

        return x

    def optimizer_setup(self, lr, lr_discount, stage = None):
        if stage is None:
            stage = self.stage
        
        settings = [
            {'params': self.mapping_network.parameters(),
                'lr' : lr * lr_discount},
            {'params': self.to_rgb.parameters(),
                'lr' : lr * lr_discount},
        ]

        if stage == 0:
            lr_add_discount = 1
        else:
            lr_add_discount = 1e-2
        settings.extend([{'params': self.initial_block.const.parameters(),
                'lr': lr * lr_add_discount},
            {'params': self.initial_block.b1.parameters(),
                'lr': lr * lr_add_discount},
            {'params': self.initial_block.a1.parameters(),
                'lr': lr * lr_add_discount},
            {'params': self.initial_block.conv.parameters(),
                'lr' : lr * lr_discount * lr_add_discount},
            {'params': self.initial_block.b2.parameters(),
                'lr': lr * lr_add_discount},
            {'params': self.initial_block.a2.parameters(),
                'lr': lr * lr_add_discount}
        ])
        for i, block in enumerate(self.blocks[:stage]):
            if stage == (i + 1):
                lr_add_discount = 1
            else:
                lr_add_discount = 1e-2
            
            settings.append({
                'params': block.conv1.parameters(),
                'lr' : lr * lr_discount * lr_add_discount})
            settings.append({
                'params': block.b1.parameters(),
                'lr': lr * lr_add_discount})
            settings.append({
                'params': block.a1.parameters(),
                'lr': lr * 1e2 * lr_add_discount})

            settings.append({
                'params': block.conv2.parameters(),
                'lr' : lr * lr_discount * lr_add_discount})
            settings.append({
                'params': block.b2.parameters(),
                'lr': lr * lr_add_discount})
            settings.append({
                'params': block.a2.parameters(),
                'lr': lr * 1e2 * lr_add_discount})
        
        return settings
